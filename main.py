import requests
import json

if __name__ == '__main__':
    url = 'https://dog.ceo/api/breeds/list/all'
    response = requests.get(url)

    if response.status_code == 200:
        print('Guardando contenido...')

        jscont = response.content
        print(jscont)

        file = open('dog.html', 'wb')
        file.write(jscont)
        file.close()